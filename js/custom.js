$(document).ready(function() {

    /* Validation Start */
    $("#mobileNumber").on("keyup", function(event) {
        if (event.keyCode >= 48 && event.keyCode <= 57) {
            if (this.value.length == 10) {
                if ($(".mobNumberInput .mdl-textfield__error").css("visibility") == "visible")
                    invalidNumber();
                else
                    validNumber();
            } else {
                invalidNumber();
            }
        } else {
            invalidNumber();
        }
    });

    $("#mobileNumber").on("blur", function(event) {
        if (this.value.length < 10) {
            if (!($("#validationIcon").hasClass("invalidNumber"))) {
                $("#validationIcon").addClass("invalidNumber");
            }
        }
    });
    $("#amount").on("keyup", function(event) {
        var regexp = new RegExp("[^a-z|^A-Z]");
        if ($(this).parent().siblings(".mobNumberInput").children("#validationIcon").hasClass("validNumber")) {
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 8) || (event.keyCode == 46)) {
                if ((parseInt(this.value) !== 0) && (this.value !== "") && (/^\d+$/.test(this.value))) {
                    if ($("#proceedLink").hasClass("mdl-button--disabled"))
                        $("#proceedLink").removeClass("mdl-button--disabled");
                } else {
                    if (!($("#proceedLink").hasClass("mdl-button--disabled")))
                        $("#proceedLink").addClass("mdl-button--disabled");
                }
            } else {
                if (!($("#proceedLink").hasClass("mdl-button--disabled")))
                    $("#proceedLink").addClass("mdl-button--disabled");
            }
        }
    });

    function validNumber() {
        if (!($("#validationIcon").hasClass("validNumber")))
            $("#validationIcon").addClass("validNumber");
        if (($("#validationIcon").hasClass("invalidNumber")))
            $("#validationIcon").removeClass("invalidNumber");
        if ($("#custCard_validation a").hasClass("disabledLinks")) {
            $("#custCard_validation a").removeClass("disabledLinks");
        }
        if (!($("#custCard_validation a").hasClass("enabledLinks"))) {
            $("#custCard_validation a").addClass("enabledLinks");
        }
        $("#custCard_validation a").css("pointer-events", "auto");
    }

    function invalidNumber() {
        if ($("#validationIcon").hasClass("validNumber"))
            $("#validationIcon").removeClass("validNumber");
        if (!($("#validationIcon").hasClass("invalidNumber")))
            $("#validationIcon").addClass("invalidNumber");
        if ($("#custCard_validation a").hasClass("enabledLinks"))
            $("#custCard_validation a").removeClass("enabledLinks");
        if (!($("#custCard_validation a").hasClass("disabledLinks")))
            $("#custCard_validation a").addClass("disabledLinks");
        $("#custCard_validation a").css("pointer-events", "none");
    }
    /* Validation End */

    /* Carousel Initiation Start */
    $('#uni-carousel1').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        swipe: true,
        touchMove: true,
        //dots: true,
        autoplay: false,
        prevArrow: "<img class='slick-prev' src='images/previous-arrow.png'>",
        nextArrow: "<img class='slick-next' src='images/next-arrow.png'>",

        responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 2,
                    // dots: true,
                    infinite: false,
                    variableWidth: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: "40px",
                    slidesToShow: 1,
                    //  dots: true,
                    infinite: false
                }
            }
        ]
    });
    /* Carousel Initiation End */

});